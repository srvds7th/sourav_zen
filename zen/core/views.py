from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
# Create your views here.
import sqlite3
import pdb
import json


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


connection = sqlite3.connect("main.db")
connection.row_factory = dict_factory

cursor = connection.cursor()

cursor.execute("select name, rank, price_usd, percent_change_24h, market_cap_usd, available_supply, volume_usd_24h, max(last_updated) from ticker_info group by name order by rank")


data = cursor.fetchall()

connection.close()


def hello_view(request):
    print(data)
    return HttpResponse("Hello World")


def home_view(request):
    return JsonResponse({'data': data})


def details_view(request):
    # pdb.set_trace()
    req_body = json.loads(request.body.decode('utf-8'))
    name = req_body['name']
    connection = sqlite3.connect("main.db")
    connection.row_factory = dict_factory
    cursor = connection.cursor()
    cursor.execute("select price_usd, market_cap_usd, volume_usd_24h, last_updated from ticker_info where name = \""+name+"\"")
    data = cursor.fetchall()
    connection.close()
    return JsonResponse({'data': data})


# 'name': 'Ardor',
#    'percent_change_1h': 3.72,
#     'percent_change_24h': 19.29,
#      'volume_usd_24h': 4389360,
#       'symbol': 'ARDR',
#        'currency_id': 'ardor',
#         'market_cap_usd': 266997600,
#          'price_btc': 4.673e-05,
#           'price_usd': 0.267265,
#            'last_updated': 1508013254,
#             'total_supply': 998999495,
#              'percent_change_7d': 35.42,
#               'ID': 4769724,
#                'rank': 24,
#                 'available_supply': 998999495


# "select price_usd, market_cap_usd, volume_usd_24h, last_updated from ticker_info where name = \""+name+"\""