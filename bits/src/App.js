import React, { Component } from 'react';
import './App.css';
import Markets from './containers/Markets'
class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Zen of the Shaolin</h1>
        </header>
        <Markets />
      </div>
    );
  }
}

export default App;
