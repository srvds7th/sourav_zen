import React, {Component} from 'react';
import axios from 'axios';
import {Link, Route} from 'react-router-dom';
import Chartist from 'chartist';


class Markets extends Component {
    state={
      data: null,
      detailsData: null,
      show: false,
      price: false,
      maxCap: false,
      volume: false,
      oneDay: false,
      week: false,
      month: false
    }
    loadComponent() {
      axios.get('http://127.0.0.1:5000/api/home/').then(response => {
        let data = response.data.data;
        console.log(data);
        this.setState({data: data});
      }).catch(error => {
        alert("Something went wrong..");
        console.log(error)
      });
    }
    componentDidMount() {
      this.loadComponent();
    }
    clickHandler = (name) => {
      console.log(name);
      let data = {'name': name}
      axios.post('http://127.0.0.1:5000/api/details/', data).then(response => {
        let detailsData = response.data.data;
        console.log(detailsData);
        this.setState({detailsData: detailsData, price: true, oneDay: true, show: true})
      }).catch(error => {
        alert("Sorry something went wrong..")
        console.log(error)
      });
    }

    render () {
      let rawData = [];
      let labels = [];
      let series = [];
      let graph = null;
      let currentTime = (new Date).getTime();
      if(this.state.show){
        if(this.state.oneDay){
          for (let i in this.state.detailsData){
            if(this.state.detailsData[i].last_updated >= currentTime-(24*60*60*1000)){
                rawData.push(this.state.detailsData[i]);
            }
          } 
        }
        else if(this.state.week){
            for (let i in this.state.detailsData){
              if(this.state.detailsData[i].last_updated >= currentTime-(7*24*60*60*1000)){
                  rawData.push(this.state.detailsData[i]);
            }
          } 
        }
        else if(this.state.month){
            for (let i in this.state.detailsData){
              if(this.state.detailsData[i].last_updated >= currentTime-(30*24*60*60*1000)){
                  rawData.push(this.state.detailsData[i]);
            }
          } 
        }
        
        
        if(this.state.price){
            for (let i in rawData){
                series.push(i.price_usd);
                labels.push(i.last_updated);
            }
        }
        else if(this.state.maxCap){
            for (let i in rawData){
                series.push(i.market_cap_usd);
                labels.push(i.last_updated);
            }
        }
        else if(this.state.volume){
            for (let i in rawData){
                series.push(i.price_usd);
                labels.push(i.Volume);
            }
        }
      }
      graph = new Chartist.Line('.ct-chart', {
              labels: labels,
              series: series,
              },
              {
                fullWidth: true,
                chartPadding: {
                    right: 40
                }
              });
      let tableData = [];
      if(this.state.data){
        let minData = this.state.data;
        for (let index in minData) {
          tableData.push(
            <tr>
              <button onClick={() => {this.clickHandler(this.state.data[index].name)}}><td>{this.state.data[index].name}</td></button>
              <td>{this.state.data[index].percent_change_24h}</td>
              <td>{this.state.data[index].market_cap_usd}</td>
              <td>{this.state.data[index].price_usd}</td>
              <td>{this.state.data[index].available_supply}</td>
              <td>{this.state.data[index].volume_usd_24h}</td>
            </tr>
          );
        }
      }
      return (
          <div className="container"><h2>Market</h2>
            <table className="table">
              <thead>
                <tr>
                  <th>Currency</th>
                  <th>1day Change %</th>
                  <th>Market Cap</th>
                  <th>Price USD</th>
                  <th>Supply</th>
                  <th>Volume</th>
                </tr>
              </thead>
              <tbody>
                {tableData}
              </tbody>
            </table>
            {graph}
          </div>
      );
    }
}


export default Markets